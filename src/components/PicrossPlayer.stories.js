import PicrossPlayer from './PicrossPlayer.vue';
import picrossData from '../../public/data/picross.json';
import { action } from '@storybook/addon-actions';

export default {
  title: 'components/PicrossPlayer',
  component: PicrossPlayer,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PicrossPlayer },
  template: '<picross-player @victory="onClick" v-bind="$props" />',
  parameters: { actions: { argTypesRegex: '^on.*' } },
  methods: {
    onClick: action('victory')
  }
});

export const defaultStory = Template.bind({});
defaultStory.storyName = "easy one 4x4"
defaultStory.args = {
  picrossData: picrossData[0]
}

export const kirbyStory = Template.bind({});
kirbyStory.storyName = "kirby"
kirbyStory.args = {
  picrossData: picrossData[3]
}
