// https://docs.cypress.io/api/introduction/api.html

describe('Troisième scénario', () => {
  it('Ouvre un picross', () => {
    cy.visit('/')
    cy.get('[href="#/game"]').click()
    cy.get('ul > li:first').click()
    cy.get('#app').toMatchImageSnapshot({
      threshold: 0.001
    })
  })
})
