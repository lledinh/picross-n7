// https://docs.cypress.io/api/introduction/api.html

describe('Premier scénario', () => {
  it('Ouvre la page d\'accueil et vérifie l\'image', () => {
    cy.visit('/')
    cy.contains('h1', 'Bienvenue sur l\'app nonogramme en Vue.js !')
    cy.wait(500)
    cy.get('#app').toMatchImageSnapshot({
      threshold: 0.001
    })
  })
})
