// https://docs.cypress.io/api/introduction/api.html

describe('Quatrième scénario', () => {
  it('Finis un picross', () => {
    cy.visit('/')
    cy.get('[href="#/game"]').click()
    cy.get('ul > li:first').click()
    const cellsEnabled = [
      { row: 0, col: 0 },
      { row: 1, col: 2 },
      { row: 1, col: 3 },
      { row: 2, col: 1 },
      { row: 2, col: 2 },
      { row: 3, col: 1 }
    ]
    cellsEnabled.forEach(cellEnabled => {
      cy.get(`table tr:nth-child(${cellEnabled.row + 2}) td:nth-child(${cellEnabled.col + 2})`).click()
    })
    cy.get('#app').toMatchImageSnapshot({
      threshold: 0.001
    })
  })
})
